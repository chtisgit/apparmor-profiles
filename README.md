# apparmor profiles

apparmor-profiles that I wrote or extended.

## Mode flags

Those profiles that are working will be set to enforcing mode.
If I'm not sure about a profile, I will upload it with complain flag and
test it.

## Use & Contributions

If you use these profiles and encounter problems with them, I would like to
hear about that and solve these problems.
If you enhance these profiles or fix them, consider letting me know about
that. It's not required (or maybe it is, see 'License Dilemma',
but it would be nice)

## License Dilemma

I put those that I wrote myself in public domain.
Some of these files were not originally written by me, but I mark their
origin in a comment.
Some of them (e.g. those taken from Debian package apparmor-profiles)
might be GPL, so I provide you with a link where you can find and read the
GPL licenses: http://www.gnu.org/licenses/licenses.de.html
Consider, that you might not be allowed to use those files as freely as
the others.
others.
